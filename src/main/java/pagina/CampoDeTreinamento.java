package pagina;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CampoDeTreinamento {

	//aperta ctrl + space para importar o pacote do jUnit
	@Test
	public void cadastroSimples() throws InterruptedException {

		//caminho at� o driver
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Ana L�cia\\Downloads\\chromedriver.exe");

		//crt + shit + O importa a selenium
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();;

		//acessar a p�gina web no chrome
		driver.get("file:///C:/Users/Ana%20L�cia/eclipse-workspace/FabricaTest/componentes.html");

		//mapeamento da p�gina
		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		WebElement sobreNome = driver.findElement(By.id("elementosForm:sobrenome"));
		WebElement sexo = driver.findElement(By.id("elementosForm:sexo:0"));
		WebElement comida = driver.findElement(By.id("elementosForm:comidaFavorita:3"));
		WebElement sugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		WebElement cadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		
		cadastrar.click();
			
		Alert alertNome = driver.switchTo().alert();
		String textoNome = alertNome.getText();
		Assert.assertEquals("Nome eh obrigatorio", textoNome);
		alertNome.accept();
		
		driver.switchTo().window("");//voltar o foco para a p�gina
		
		//a��es realizadas atrav�s dos m�todos da biblioteca Selenium WebElement
		nome.clear();
		nome.sendKeys("Kelion");

		sobreNome.clear();
		sobreNome.sendKeys("Fernandes");

		sexo.click();
		
		comida.click();

		//driver.quit();		

		WebElement element = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(element);
		combo.selectByValue("superior");

		Assert.assertEquals("Superior", combo.getFirstSelectedOption().getText());

		WebElement element2 = driver.findElement(By.id("elementosForm:esportes"));
		Select combo2 = new Select(element2);
		combo2.selectByValue("nada");

		Assert.assertEquals("O que eh esporte?", combo2.getFirstSelectedOption().getText());

		sugestoes.clear();
		sugestoes.sendKeys("HIAHDWBDIBWQCBOQBWBQOWBWQDIDBWQIDBQDVBWQUOVDWQDBIWQDBWQIYDBVWQOYVDOWQID");

		WebElement clickAlert = driver.findElement(By.xpath("//table[@id='elementosForm:tableUsuarios']//tr[1]/td[3]/input"));
		
		clickAlert.click();

		Alert alert = driver.switchTo().alert();
		String texto = alert.getText();
		Assert.assertEquals("Francisco", texto);
		alert.accept();
		
		cadastrar.click();
		
		WebElement nomeCad = driver.findElement(By.xpath("descNome"));
		
		Assert.assertEquals("Kelion", nomeCad);
		
	}
}

